FROM python:3
COPY . /
WORKDIR /
RUN pip install --upgrade pip
RUN pip install --upgrade cython
RUN pip install -r requirements.txt
RUN python -m spacy download en

CMD [ "python", "./json_parser.py" ]
