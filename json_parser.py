import spacy
import json
import requests
import re
from datetime import datetime

global_name_sets = set()
redact_text = "---"
spacy_nlp = spacy.load('en_core_web_sm')
feed_data_url = 'http://therecord.co/feed.json'
feed_data_dict = requests.get(feed_data_url).json()
filename = "result.json"

# clean up the html tag from text
def clear_html_tag(html_content):
    reg = re.compile('<.*?>')
    clean_text = re.sub(reg, '', html_content)
    return clean_text


# extract a set of name from text
def name_extractor(text):
    doc = spacy_nlp(text.strip())
    named_entities = set()
    for i in doc.ents:
        entry = str(i.lemma_).lower()
        text = text.replace(str(i).lower(), "")
        if i.label_ in ["ART", "EVE", "NAT", "PERSON"]:
            named_entities.add(entry.title())
            for n in entry.split():
                global_name_sets.add(n.title())
                global_name_sets.add(n.lower())
    return named_entities


# process text value from the dictionary data extracted from json
def dict_scanner(dict_var):
    for k, v in dict_var.items():
        if isinstance(v, str):
            clear_text = clear_html_tag(v)
            name_extractor(clear_text)
            v_readact = v
            for name in global_name_sets:
                v_readact = v_readact.replace(name, redact_text)
            dict_var[k] = v_readact
        elif isinstance(v, dict):
            dict_scanner(v)
        elif isinstance(v, list):
            for item in v:
                if isinstance(item, dict):
                    dict_scanner(item)

dict_scanner(feed_data_dict)
feed_data_dict['redacted'] = str(datetime.now())

with open(filename, 'w', encoding='utf8') as json_file:
    res = json.dumps(feed_data_dict, indent=4, ensure_ascii=False)
    print(res, file=json_file)

