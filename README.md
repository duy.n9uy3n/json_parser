# json_parser

json_parser.py:
a simple python script that 
    1/ parse json data from http://therecord.co/feed.json
    2/ redact all occurrences of names of people in the text
    3/ return a json document 

Dockerfile: 
    To dockerize the above Python application as docker image
    Use the command "docker build -t json-parser ." to build docker image named json-parser for instance


json-parser-deployment.yaml: 
    Deployment manifest in K8S cluster
    Using the "kubectl apply -f json-parser-deployment.yaml" for deploying service 
    Assuming that the docker image for json_parser has been pulished

response:
    Include the response to Q2 about CI strategy and Q3 about zero-downtime when upgrading service 
    
    
How to ensure zero-downtime when ugrading service?
    By running our application using an Deployment resources in the K8S cluster we can ensure the zero-downtime when do upgrading the service.
    The rolling update strategy (available by default for Deployment resource) allow us to do that
    Once we update a new app version by modifying the manifest then appply or kubectl edit , etc.. Kubernetes automatically handles the removes of old pods one by one while adding new ones at the same time

